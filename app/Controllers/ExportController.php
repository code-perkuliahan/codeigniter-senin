<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ContentModel;
use PhpOffice\PhpSpreadsheet\Reader\Xls\Style\Border;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border as StyleBorder;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportController extends BaseController
{
    protected $contentModel;

    public function __construct()
    {
        $this->contentModel = new ContentModel();
    }
    
    public function index()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->setActiveSheetIndex(0);

        $style_col = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                //UNTUK SET BORDER BERDASARKAN SISI SETIAP CELL
                /* 'top' => [
                    'borderStyle' => StyleBorder::BORDER_THIN
                ],
                'bottom' => [
                    'borderStyle' => StyleBorder::BORDER_THIN
                ],
                'right' => [
                    'borderStyle' => StyleBorder::BORDER_THIN
                ],
                'left' => [
                    'borderStyle' => StyleBorder::BORDER_THIN
                ] */

                //UNTUK SET BORDER BERDASARKAN DI SELURUH CELL
                'allBorders' => [
                    'borderStyle' => StyleBorder::BORDER_THIN
                ]
            ]
        ];

        $sheet->setCellValue('A2','Judul')
        ->setCellValue('B2','Tanggal Dibuat')
        ->setCellValue('C2','Kategori')
        ->setCellValue('D2','Status');

        $sheet->getStyle('A2')->applyFromArray($style_col);
        $sheet->getStyle('B2')->applyFromArray($style_col);
        $sheet->getStyle('C2')->applyFromArray($style_col);
        $sheet->getStyle('D2')->applyFromArray($style_col);

        $sheet->setCellValue('A1','DATA BERITA TERBARU');
        $sheet->mergeCells('A1:D1');
        $sheet->getStyle('A1')->getFont()->setBold(true);
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:D1')->getBorders()->getAllBorders()->setBorderStyle(StyleBorder::BORDER_THICK);

        $column = 3;
        $content = $this->contentModel->getListContent();
        foreach($content as $dt)
        {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$column, $dt->title)
                ->setCellValue('B'.$column, $dt->tanggal)
                ->setCellValue('C'.$column, $dt->nama_kategori)
                ->setCellValue('D'.$column, $dt->nama_status);
            $column++;
        }

        $writer = new Xlsx($spreadsheet);
        $filename = date('Y-m-dHis').'pwl_senin';

        header('Content-Type:application/vnd.ms_excel'); //application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
        header('Content-Disposition:attachment; filename='.$filename.'.xlsx');
        header('Cache-Control:max-age=0');

        $writer->save('php://output');
    }
}