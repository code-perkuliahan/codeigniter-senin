<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ContentModel;
use App\Models\MasterCategory;

class ReportController extends BaseController
{
    protected $contentModel;
    protected $masterCategory;

    public function __construct()
    {
        $this->contentModel = new ContentModel();
        $this->masterCategory = new MasterCategory();
    }
    
    public function index()
    {
        $dataPoints = $barPoints = $periode = [];
        $totalcontent = $this->contentModel->countAllResults();
        $totalCategory = $this->masterCategory->countAllResults();
        $content = $this->contentModel->getTotalKategori();

        //UNTUK GET DATA COLUMN-CHART
        $listStatus = $this->contentModel->getListStatus();
        $getPeriode = $this->contentModel->getPeriode();


        foreach ($content as $data) {
			$dataPoints[] = [
				"name" => $data->nama_kategori,
				"y" => floatval($data->total)
			];
		}

        //UNTUK GET DATA COLUMN-CHART (MENGGUNAKAN NESTED ARRAY)
        foreach ($getPeriode as $key) {
            $periode[] = $key->bulan;
        }
        foreach ($listStatus as $v) {
            unset($barchart);
            $barchart = array();
            foreach ($getPeriode as $key) {
                $barcharts = $this->contentModel->getTotalStatusContent($key->bulan, $v->id);
                if (isset($barcharts)) {
                    $barchart[] = intval($barcharts->total);
                }else{
                    $barchart[] = 0;
                }
            }
            $barPoints[] = [
                "name" => $v->nama_status,
                "data" => $barchart
            ];
        }

        return view("dashboard_chart", [
			"data" => json_encode($dataPoints),
            'bars' => json_encode($barPoints),
            'bulan' => json_encode($periode),
            'totalContents' => $totalcontent,
            'totalCategory' => $totalCategory
		]);
    }
}
