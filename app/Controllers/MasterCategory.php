<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ContentModel;
use App\Models\MasterCategory as ModelsMasterCategory;

class MasterCategory extends BaseController
{
    protected $categoryModels;
    
    public function __construct()
    {
        $this->categoryModels = new ModelsMasterCategory();
    }
    public function index()
    {
        $data['contents'] = $this->categoryModels->orderBy('nama_kategori', 'desc')->findAll();

        return view('admin/list_kategori', $data);
    }

    public function create()
    {
        #BAGIAN UNTUK SUBMIT BERITA
        if ($this->request->getMethod() == 'post') {
            $data_contents = [
                'nama_kategori' => $this->request->getPost('nama_kategori'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->full_name
            ];
            $is_saved = $this->categoryModels->insert($data_contents);
            if ($is_saved) {
                return redirect('admin/category');
            }
        }

        return view('admin/add_kategori');
    }

    public function update($kategori_id)
    {
        $data['kategori'] = $this->categoryModels->where('id', $kategori_id)->first();
        #BAGIAN UNTUK SUBMIT BERITA
        if ($this->request->getMethod() == 'post') {
            $data_contents = [
                'nama_kategori' => $this->request->getPost('nama_kategori')
            ];
            $is_saved = $this->categoryModels->update($kategori_id, $data_contents);
            if ($is_saved) {
                return redirect('admin/category');
            }
        }

        return view('admin/edit_kategori', $data);
    }

    public function delete($kategori_id)
    {
        #JIKA HARD DELETE, DATA YANG DIHAPUS AKAN HILANG
        $this->categoryModels->delete($kategori_id);
        #JIKA SOFT DELETE, DATA YANG DIHAPUS AKAN DI UPDATE STATUS
        /* $data_contents = [
            'deleted_at' => date('Y-m-d H:i:s')
        ];
        $this->categoryModels->update($kategori_id, $data_contents); */
        
        return redirect('admin/category');
    }
}
