<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ContentModel;
use App\Models\MasterCategory;
use App\Models\UserModel;

class AuthController extends BaseController
{
    protected $userModel;
    protected $contentModel;
    protected $masterCategory;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->contentModel = new ContentModel();
        $this->masterCategory = new MasterCategory();
    }

    public function index()
    {
        $session = session();
        $bulan = date('m');

        $dataPoints = [];
        $totalcontent = $this->contentModel->where("date_format(tanggal,'%m')", $bulan)
            ->where('draft', 1)->countAllResults();        
        $totalKategori = $this->contentModel->getKategoriBasedPeriode();
        $content = $this->contentModel->getTotalKategoriBasedPeriode();
        foreach ($content as $data) {
			$dataPoints[] = [
				"name" => $data->nama_kategori,
				"y" => floatval($data->total)
			];
		}
        
        return view('admin/dashboard', [
            'session' => $session, "piechart" => json_encode($dataPoints),
            'totalContent' => $totalcontent, 'totalKategori' => $totalKategori]
        );
    }

    public function login()
    {
        return view('auth/login');
    }
 
    public function auth()
    {
        $session = session();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $data = $this->userModel->where('email', $email)->first();
        if($data){
            if($data->active){
                $pass = $data->password;
                $verify_pass = password_verify($password, $pass);
                if($verify_pass){
                    $ses_data = [
                        'user_id'       => $data->id,
                        'full_name'     => $data->name,
                        'user_name'     => $data->username,
                        'user_email'    => $data->email,
                        'logged_in'     => true,
                    ];
                    $session->set($ses_data);
                    return redirect()->to('admin');
                }else{
                    $session->setFlashdata('msg', 'Wrong Password');
                    return redirect('admin/login');
                }
            }else{
                $session->setFlashdata('msg', 'Data already in-active, due to user resign');
                return redirect('admin/login');
            }
        }else{
            $session->setFlashdata('msg', 'Email not Found');
            return redirect('admin/login');
        }
    }
 
    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('admin/login');
    }

    public function register()
    {
        $data = [];
        return view('auth/register', $data);
    }

    public function register_auth()
    {
        $rules = [
            'name'          => 'required|min_length[3]|max_length[20]',
            'email'         => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
            'password'      => 'required|min_length[6]|max_length[200]',
            'confpassword'  => 'matches[password]'
        ];
         
        if($this->validate($rules)){
            $data = [
                'name'     => $this->request->getPost('name'),
                'username' => strtolower($this->request->getVar('name')),
                'email'    => $this->request->getPost('email'),
                'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT)
            ];
            $this->userModel->save($data);
            return redirect()->to('admin/login');
        }else{
            $data['validation'] = $this->validator;
            return view('auth/register', $data);
        }
    }
}
