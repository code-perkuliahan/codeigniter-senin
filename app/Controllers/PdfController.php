<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ContentModel;
use Dompdf\Dompdf;

class PdfController extends BaseController
{
    protected $contentModel;

    public function __construct()
    {
        $this->contentModel = new ContentModel();
    }
    
    public function index()
    {
        $filename = date('Y-m-d-H-i-s').'-pwl';
        $berita = $this->contentModel->getDetailContent(5);

        #QUERY DETAIL KONTEN MENGGUNAKAN QUERY RESULT
        $contents = $this->contentModel->getDetailBerita(5);

        $dompdf = new Dompdf();
        $dompdf->getOptions()->setChroot(FCPATH.'adminlte\img');
        $dompdf->loadHtml(view('export_pdf', ['berita' => $berita, 'contents' => $contents]));
        $dompdf->setPaper('A4','portrait');
        $dompdf->render();


        $dompdf->stream($filename);
    }
}
