<?php

namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\ContentModel;
use App\Models\MasterCategory;
use CodeIgniter\Exceptions\PageNotFoundException;

class Home extends BaseController
{
    protected $contentModel;
    protected $categoryModel;

    public function __construct()
    {
        $this->contentModel = new ContentModel();
        $this->categoryModel = new MasterCategory();
    }

    public function index(): string
    {
        $detail['nama_mhs'] = 'Mahasiswa 0001'; #UNTUK PASSING VARIABLE SECARA HARDCODE / DEFAULT
        $detail['nama_mhs'] = $this->request->getGet('nama_mhs'); #UNTUK PASSING VARIABLE SECARA DINAMIS DARI GET PARAM
        // data array tanpa key
        $detail['list_mhs'] = ['Mahasiswa 003', 'Mahasiswa 005', 'Mahasiswa 007', 'Mahasiswa 009', 'Mahasiswa 010'];

        // data array dengan menggunakan key -- untuk informasi news
        $detail['data_news'] = [
            [
                'title' => 'Apa itu Codeigniter?',
                'content' => 'Codeigniter adalah framework untuk membuat web',
                'tahun' => '2023'
            ],
            [
                'title' => 'Aplikasi berbasis Codeigniter?',
                'content' => 'Codeigniter merupakan framework untuk membuat web',
            ],
            [
                'title' => 'Codeigneter v4',
                'content' => 'Codeigniter menjadikan framework untuk membuat web',
                'tahun' => '2022'
            ]

        ];

        return view('welcome_message', $detail);
    }

    public function detail()
    {
        #GET LIST DATA KONTEN MENGGUNAKAN MODELS
        $data['contents'] = $this->contentModel->orderBy('tanggal', 'desc')->findAll();

        #GET LIST DATA KONTEN MENGGUNAKAN QUERY TABLE
        $data['berita'] = $this->db->table('contents')->get()->getResult();
        #UNTUK LIST CATEGORY
        #$data['list_category'] = ['Mahasiswa 003', 'Mahasiswa 005', 'Mahasiswa 007', 'Mahasiswa 009', 'Mahasiswa 010'];
        $data['list_category'] = $this->categoryModel->get_category();
        $data['list_status'] = $this->contentModel->getStatusKonten();

        #BAGIAN UNTUK SUBMIT BERITA
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
            ];

            $messages = [
                'file_upload' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File extention harus berupa docx,pdf'
                ]
            ];

            if ($this->validate($rules, $messages)) {
                $dataBerkas = $this->request->getFile('file_upload');
                $fileName = $dataBerkas->getRandomName();
                $data_contents = [
                    'title' => $this->request->getPost('title'),
                    'content' => $this->request->getPost('content'),
                    'file_upload' => $fileName,
                    'kategori' => $this->request->getVar('kategori'),
                    'tanggal' => date('Y-m-d H:i:s'), 'draft' => $this->request->getVar('status_berita')
                ];
                $dataBerkas->move('uploads/', $fileName);
                $is_saved = $this->contentModel->insert($data_contents);
                if ($is_saved) {
                    return redirect('about');
                }
            } else {
                $data['validation'] = $this->validator;
                return view('welcome_home', $data);
            }
        }

        return view('welcome_home', $data);
    }

    public function profile()
    {
        $data['username'] = '';
        if ($this->request->getMethod() == 'post') {
            #UNTUK SAVE DATA TO DATABASE
            $profile = [
                'name' => $this->request->getPost('name'),
                'email' => $this->request->getPost('email'),
                'message' => $this->request->getPost('message')
            ];

            $profile_saved = $this->contentModel->insert($profile);

            if ($profile_saved) {
                return redirect('profile');
            }

            #UNTUK LOAD DATA FROM SUBMIT
            #$data['username'] = $this->request->getPost('username');
            #return view('profile', $data);
        }
        return view('profile', $data);
    }

    public function detailContent($content_id)
    {
        #QUERY DETAIL KONTEN MENGGUNAKAN QUERY BUILDER
        $data['berita'] = $this->contentModel->getDetailContent($content_id);

        #QUERY DETAIL KONTEN MENGGUNAKAN QUERY RESULT
        $data['contents'] = $this->contentModel->getDetailBerita($content_id);
        
        // tampilkan 404 error jika data tidak ditemukan
        if (!$data['berita']) {
            throw PageNotFoundException::forPageNotFound();
        }

        return view('detail_berita', $data);
    }

    public function updateBerita($content_id)
    {
        $data['berita'] = $this->contentModel->where(['id' => $content_id])->first();

        // tampilkan 404 error jika data tidak ditemukan
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
            ];

            $messages = [
                'file_upload' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File extention harus berupa docx,pdf'
                ]
            ];
            if ($this->validate($rules, $messages)) {
                $details = $this->contentModel->find($content_id);
                unlink('uploads/'.$details['file_upload']);

                $dataBerkas = $this->request->getFile('file_upload');
                $fileName = $dataBerkas->getRandomName();
                
                $this->contentModel->update($content_id, [
                    "title" => $this->request->getPost('title'),
                    "content" => $this->request->getPost('content'),
                    'file_upload' => $fileName,
                ]);
                $dataBerkas->move('uploads/', $fileName);
                return redirect('about');
            } else {
                $data['validation'] = $this->validator;
                return view('welcome_home', $data);
            }
        }

        return view('edit_berita', $data);
    }

    public function downloadFile($content_id)
    {
        $data = $this->contentModel->find($content_id);
        return $this->response->download('uploads/' . $data['file_upload'], null);
    }

    public function deleteBerita ($content_id)
    {
        $data = $this->contentModel->find($content_id);
        unlink('uploads/'.$data['file_upload']);
        
        $this->contentModel->where('id', $content_id)->delete();

        return redirect('about');
    }

    public function tambahBerita()
    {
        $rules = [
            'file_upload'   => 'uploaded[file_upload]|mime_in[file_upload,application/pdf,application/word]'
        ];

        $messages = [
            'file_upload' => [
                'uploaded' => 'Harus Ada File yang diupload',
                'mime_in' => 'File extention harus berupa docx,pdf'
            ]
        ];

        if ($this->validate($rules, $messages)) {
            $dataBerkas = $this->request->getFile('file_upload');
            $fileName = $dataBerkas->getRandomName();
            $data_contents = [
                'title' => $this->request->getPost('title'),
                'content' => $this->request->getPost('content'),
                'file_upload' => $fileName,
                'kategori' => $this->request->getVar('kategori'),
                'tanggal' => date('Y-m-d H:i:s'), 'draft' => $this->request->getVar('status_berita')
            ];
            $dataBerkas->move('uploads/', $fileName);
            $is_saved = $this->contentModel->insert($data_contents);
            if ($is_saved) {
                return $this->response->setJSON([
                    'error' => false,
                    'message' => 'Successfully added new post!'
                ]);
            }
        } else {
            return $this->response->setJSON([
                'error' => true,
                'message' => $this->validator->getErrors()
            ]);
        }
    }
}
