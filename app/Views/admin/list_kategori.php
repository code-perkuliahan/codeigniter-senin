<?= $this->extend('layouts/admin_layout') ?>
<?= $this->section('content') ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-tools">
                            <a href="<?= base_url('admin/category/new') ?>" class="btn btn-success btn-sm pull-right"><i class="fa fa-pencil"></i> Tambah Kategori </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="myTable" class="table table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <th scope="col" class="detail-col">No</th>
                                <th scope="col" class="detail-col">Category Name</th>
                                <th scope="col" class="detail-col">Created Date</th>
                                <th scope="col" class="detail-col">Created By</th>
                                <th scope="col" class="detail-col">Action</th>
                            </thead>
                            <tbody>
                                <?php foreach($contents as $list) : ?>
                                <tr>
                                    <td><?= $list['id'] ?></td>
                                    <td><?= $list['nama_kategori'] ?></td>
                                    <td><?= $list['created_at'] ?></td>
                                    <td><?= $list['created_by'] ?></td>
                                    <td><a href="<?= base_url('admin/category/'.$list['id'].'/edit')?>" class="btn btn-sm btn-outline-warning">Edit</a>
                                    <a href="<?= base_url('admin/category/'.$list['id'].'/delete')?>" class="btn btn-sm btn-outline-danger">Delete</a></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>