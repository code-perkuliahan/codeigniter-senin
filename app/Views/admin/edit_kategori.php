<?= $this->extend('layouts/admin_layout') ?>
<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-tools">
                        Edit Kategori
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" class="form">
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" name="nama_kategori" class="form-control" maxlength="100" value="<?= $kategori['nama_kategori'] ?>">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Ubah Data" class="btn btn-primary w-100">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>