<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<?php if (isset($validation)) : ?>
				<div class="alert alert-danger"><?= $validation->listErrors() ?></div>
			<?php endif; ?>
			<form method="post" class="form" enctype="multipart/form-data" id="add_content" novalidate>
				<div class="form-group">
					<label>Judul Berita</label>
					<input type="text" id="title" name="title" class="form-control" maxlength="100" value="<?= set_value('title') ?>" required>
				</div>
				<div class="form-group">
					<label>Isi Berita</label>
					<textarea name="content" class="form-control" cols="30" rows="10" required><?= set_value('content') ?></textarea>
				</div>
				<div class="form-group">
					<label>Kategori Berita</label>
					<?= form_dropdown('kategori', $list_category, set_value('kategori'), ['class' => 'form-control', 'required' => true, 'id' => 'kategori']) ?>
				</div>
				<div class="form-group">
					<label>Status Berita</label>
					<?= form_dropdown('status_berita', $list_status, set_value('status_berita'), ['class' => 'form-control', 'required' => true, 'id'=> 'status_berita']) ?>
				</div>
				<div class="form-group">
					<label for="berkas" class="form-label">File Attachment</label>
					<input type="file" class="form-control" id="file_upload" name="file_upload" required>
				</div>
				<div class="form-group">
					<button type="submit" id="add_post_btn" class="btn btn-primary w-100">Tambah Berita</button>
				</div>
			</form>
		</div>
		<div class="col-md-4">
			<!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
			<a href="<?= base_url('export_excel') ?>" class="btn btn-sm btn-outline-warning">Export Excel</a>
			<a href="<?= base_url('dashboard_report') ?>" class="btn btn-sm btn-outline-danger">Dashboard Report</a>
			<?php foreach ($contents as $news) : ?>
				<div class="col-md-12 my-2 card">
					<div class="card-body">
						<h5 class="h5"><?= $news['title'] ?></h5>
						<p><?= $news['content'] ?></p>
						<a href="<?= base_url('about/download/' . $news['id']) ?>" class="btn btn-sm btn-outline-primary">download</a>
						<a href="<?= base_url('about/delete/' . $news['id']) ?>" class="btn btn-sm btn-outline-danger">delete</a>
						<a href="<?= base_url('about/' . $news['id']) ?>" class="btn btn-sm btn-outline-secondary">more</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="col-md-4">
			<?php foreach ($berita as $dt) : ?>
				<div class="col-md-12 my-2 card">
					<div class="card-body">
						<h5 class="h5"><?= $dt->title ?></h5>
						<p><?= $dt->content ?></p>
						<a href="<?= base_url('about/download/' . $dt->id) ?>" class="btn btn-sm btn-outline-primary">download</a>
						<a href="<?= base_url('about/delete/' . $dt->id) ?>" class="btn btn-sm btn-outline-danger">delete</a>
						<a href="<?= base_url('about/' . $dt->id) ?>" class="btn btn-sm btn-outline-secondary">more</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?= $this->endSection() ?>
<?= $this->section('lib-js') ?>
<script>
	jQuery(function($) {
		$("#add_content").submit(function(e) {
			e.preventDefault();
			const formData = new FormData(this);
			if (!this.checkValidity()) {
				e.preventDefault();
				$(this).addClass('was-validated');
			} else {
				$("#add_post_btn").text("On Progress...");
				$("#add_post_btn").attr("disabled", true);
				$.ajax({
					url: '<?= base_url('content/add') ?>',
					method: 'post',
					data: formData,
					contentType: false,
					cache: false,
					processData: false,
					dataType: 'json',
					success: function(response) {
						if (response.error) {
							$("#file_upload").addClass('is-invalid');
							$("#file_upload").next().text(response.message.file_upload);
							Swal.fire(
								'Error',
								response.message.file_upload,
								'error'
							)
						} else {
							$("#add_content")[0].reset();
							$("#file_upload").removeClass('is-invalid');
							$("#file_upload").next().text('');
							$("#add_content").removeClass('was-validated');
							Swal.fire(
								'Added',
								response.message,
								'success'
							).then(function() {
								window.location = "<?= base_url('about') ?>";
							});
						}
						$("#add_post_btn").text("Tambah Berita");
						$("#add_post_btn").attr("disabled", false);

					}
				});
			}
		});

		$('#kategori').on('change', function(e){
			console.log(this.value);
			if(this.value == 3){
				$("#status_berita").val(4);
			}
		});

		$('#status_berita').on('change', function(e){
			console.log(this.value);
			if(this.value == 3){
				$("#add_post_btn").attr("disabled", true);
			}else{
				$("#add_post_btn").attr("disabled", false);
			}
		});

		$('#title').on('keyup', function(e){
			var title = this.value;
			console.log(title.length);
			if(title.length >= 20){
				Swal.fire(
					'Warning',
					'Judul tidak boleh lebih dari 20 karakter',
					'warning'
				).then(function(){
					$("#add_post_btn").attr("disabled", true);
				});
			}else{
				$("#add_post_btn").attr("disabled", false);
			}
		});
	});
</script>
<?= $this->endSection() ?>