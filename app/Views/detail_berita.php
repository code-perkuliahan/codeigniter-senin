<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <?php foreach($berita as $dt) : ?>
        <div class="row">
            <!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
            <div class="col-md-12 my-2 card">
                <div class="card-body">
                    <h1><?= $dt->title ?></h1>
                    <?php if($dt->draft == 1) : ?>
                        <span class="badge badge-success"><?= $dt->nama_status ?></span>
                    <?php elseif($dt->draft == 2) : ?>
                        <span class="badge badge-warning"><?= $dt->nama_status ?></span>
                    <?php elseif($dt->draft == 3) : ?>
                        <span class="badge badge-danger"><?= $dt->nama_status ?></span>
                    <?php else : ?>
                        <span class="badge badge-secondary"><?= $dt->nama_status ?></span>
                    <?php endif; ?>
                    <h5><?= $dt->nama_kategori ?></h5>
                    <p><?= $dt->content ?></p>
                </div>
            </div>
            <a href="<?= base_url('about/edit/'.$dt->id) ?>" class="btn btn-sm btn-warning">Edit</a>
            <a href="<?= base_url('export_pdf/'.$dt->id) ?>" class="btn btn-sm btn-info">Export</a>
        </div>
        <?php endforeach; ?>

        <div class="row">
            <!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
            <div class="col-md-12 my-2 card">
                <div class="card-body">
                    <h1><?= $contents->title ?></h1>
                    <?php if($contents->draft == 1) : ?>
                        <span class="badge badge-success"><?= $contents->nama_status ?></span>
                    <?php elseif($contents->draft == 2) : ?>
                        <span class="badge badge-warning"><?= $contents->nama_status ?></span>
                    <?php elseif($contents->draft == 3) : ?>
                        <span class="badge badge-danger"><?= $contents->nama_status ?></span>
                    <?php else : ?>
                        <span class="badge badge-secondary"><?= $contents->nama_status ?></span>
                    <?php endif; ?>
                    <h5><?= $contents->nama_kategori ?></h5>
                    <p><?= $contents->content ?></p>
                </div>
            </div>
            <a href="<?= base_url('about/edit/'.$contents->id) ?>" class="btn btn-sm btn-warning">Edit</a>
        </div>
    </div>
<?= $this->endSection() ?>