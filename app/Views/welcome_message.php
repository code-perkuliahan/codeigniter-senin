<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
<div class="container">
		<div class="row">
			<!-- UNTUK LIST NEWS / CONTENT USING HTML -->
			<div class="col-md-12 my-2 card">
				<div class="card-body">
					<h5 class="h5">Codeigniter 4 Sudah Rilis!</h5>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam perferendis commodi tenetur quos ducimus repellat nulla, nam magni. Commodi iusto ad harum voluptas exercitationem facere eos earum laboriosam excepturi quas?</p>
				</div>
			</div>
			<div class="col-md-12 my-2 card">
				<div class="card-body">
					<h5 class="h5">Pengembangan Codeiginter 4 Tertunda</h5>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam perferendis commodi tenetur quos ducimus repellat nulla, nam magni. Commodi iusto ad harum voluptas exercitationem facere eos earum laboriosam excepturi quas?</p>
				</div>
			</div>
			<!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
			<?php foreach ($data_news as $news) : ?>
			<div class="col-md-12 my-2 card">
				<div class="card-body">
					<h5 class="h5"><?= $news['title'] ?></h5>
					<p><?= isset($news['tahun']) ? $news['tahun'] : 'Tidak ada' ?> - <?= $news['content'] ?></p>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
<?= $this->endSection() ?>