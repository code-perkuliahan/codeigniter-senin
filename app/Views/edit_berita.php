<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<form method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= $berita['id'] ?>" />
					<div class="form-group">
						<label>Judul Berita</label>
						<input type="text" name="title" class="form-control" maxlength="100" value="<?= $berita['title'] ?>">
					</div>
					<div class="form-group">
						<label>Isi Berita</label>
						<textarea name="content" class="form-control" cols="30" rows="10"><?= $berita['content'] ?></textarea>
					</div>
					<div class="form-group">
                        <label for="berkas" class="form-label">File Attachment</label>
                        <input type="file" class="form-control" id="file_upload" name="file_upload">
                    </div>					
					<div class="form-group">
						<input type="submit" value="Ubah Berita" class="btn btn-primary w-100">
					</div>
				</form>
			</div>
		</div>
	</div>
<?= $this->endSection() ?>