<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <?php if(!$username) : ?>
        <div class="row">
            <div class="col-md-6">
            
            <form action="" class="form" method="post">

            <div class="form-group">
                <label for="email">Username</label>
                <input type="text" class="form-control" name="username">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">Address</label>
                <textarea name="message" class="form-control" id="" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="email">Phone</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" value="Kirim" class="btn btn-primary w-100">
            </div>

            </form>

            </div>
        </div>
        <?php else : ?>
        <h1>Thank You, <?= $username ?></h1>
		<p>Your data already saved</p>
        <?php endif ?>
    </div>
    <?= $this->endSection() ?>