<?= $this->extend('layouts/page_layout') ?>
<?= $this->section('content') ?>
    <div class="container">
        <div class="row">
            <div class="col-md-2 my-2 card">
                <h3><?= $totalContents ?></h3>
                <p>Total All Contents</p>
            </div>
            <div class="col-md-2 my-2 card">
                <h3><?= $totalCategory ?></h3>
                <p>Total Category</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 my-2 card">
                <div class="card-body">
                    <div id="pie-chart"></div>
                </div>
            </div>
            <div class="col-md-6 my-2 card">
                <div class="card-body">
                    <div id="bar-chart"></div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
<?= $this->section('lib-js') ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
$(function() {
    Highcharts.chart('pie-chart', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Total Kategori'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'total',
            colorByPoint: true,
            data: <?= $data ?>
        }]
    });
    Highcharts.chart('bar-chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Total Status Berita'
        },
        xAxis: {
            categories: <?= $bulan ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Marks'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key} Marks</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: <?= $bars ?>
    });
});
</script>
<?= $this->endSection() ?>