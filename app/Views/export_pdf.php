<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal Berita</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('css/bootstrap.min.css') ?>" />
    <style>
        .badge {
            border: 1px solid #000;
            position: relative;
            top: -1px;
            display: inline-block;
            padding: 0.25em 0.4em;
            font-size: 75%;
            font-weight: 700;
            line-height: 1;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 0.25rem;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .badge-success {
            color: #fff;
            background-color: #28a745;
        }

        .badge-secondary {
            color: #fff;
            background-color: #6c757d;
        }

        .badge-warning {
            color: #212529;
            background-color: #ffc107;
        }

        .badge-danger {
            color: #fff;
            background-color: #dc3545;
        }
    </style>
</head>

<body>
<img src="<?= FCPATH.'adminlte\img\logo.png' ?>" alt="Logo" width="50px">
    <div class="container">
        <?php foreach ($berita as $dt) : ?>
            <div class="row">
                <!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
                <div class="col-md-12 my-2 card">
                    <div class="card-body">
                        <h1><?= $dt->title ?></h1>
                        <?php if ($dt->draft == 1) : ?>
                            <span class="badge badge-success"><?= $dt->nama_status ?></span>
                        <?php elseif ($dt->draft == 2) : ?>
                            <span class="badge badge-warning"><?= $dt->nama_status ?></span>
                        <?php elseif ($dt->draft == 3) : ?>
                            <span class="badge badge-danger"><?= $dt->nama_status ?></span>
                        <?php else : ?>
                            <span class="badge badge-secondary"><?= $dt->nama_status ?></span>
                        <?php endif; ?>
                        <h5><?= $dt->nama_kategori ?></h5>
                        <p><?= $dt->content ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

        <div class="row">
            <!-- UNTUK LIST NEWS / CONTENT USING ARRAY -->
            <div class="col-md-12 my-2 card">
                <div class="card-body">
                    <h1><?= $contents->title ?></h1>
                    <?php if ($contents->draft == 1) : ?>
                        <span class="badge badge-success"><?= $contents->nama_status ?></span>
                    <?php elseif ($contents->draft == 2) : ?>
                        <span class="badge badge-warning"><?= $contents->nama_status ?></span>
                    <?php elseif ($contents->draft == 3) : ?>
                        <span class="badge badge-danger"><?= $contents->nama_status ?></span>
                    <?php else : ?>
                        <span class="badge badge-secondary"><?= $contents->nama_status ?></span>
                    <?php endif; ?>
                    <h5><?= $contents->nama_kategori ?></h5>
                    <p><?= $contents->content ?></p>
                </div>
            </div>
        </div>
    </div>
</body>

</html>