<?php

namespace App\Models;

use CodeIgniter\Model;

class ContentModel extends Model
{
    protected $table            = 'contents';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $allowedFields    = ['title','content','tanggal','draft','file_upload','kategori'];

    public function getDetailContent($content_id)
    {
        $detail = $this->db->table('contents')
        ->select('contents.id, nama_kategori, nama_status, draft, title, content')
        ->join('mastercategories','mastercategories.id = contents.kategori')
        ->join('contents_status','contents_status.id = contents.draft')
        ->where('contents.id', $content_id)->get()->getResult();

        return $detail;
    }

    public function getDetailBerita($content_id)
    {
        $detail = $this->db->query('select contents.id, nama_kategori, nama_status, draft, title, content from contents inner join mastercategories on mastercategories.id = contents.kategori
            inner join contents_status on contents_status.id = contents.draft where contents.id = '.$content_id);

        $result = $detail->getRow();

        return $result;
    }

    public function getStatusKonten()
    {
        $result = [];
        $query = $this->db->table('contents_status')->get();
        foreach($query->getResult() as $key)
        {
            $result[$key->id] = $key->nama_status;
        }
        return $result;
    }

    public function getListContent()
    {
        $detail = $this->db->table('contents')
        ->select('contents.id, tanggal, nama_kategori, nama_status, draft, title, content')
        ->join('mastercategories','mastercategories.id = contents.kategori')
        ->join('contents_status','contents_status.id = contents.draft')
        ->get()->getResult();

        return $detail;
    }

    public function getTotalKategori()
    {
        $detail = $this->db->table('contents')
        ->select('nama_kategori, count(contents.id) as total')
        ->join('mastercategories','mastercategories.id = contents.kategori')
        ->groupBy('nama_kategori')
        ->get()->getResult();

        return $detail;
    }

    //UNTUK GET LIST STATUS
    public function getListStatus()
    {
        $result = $this->db->table('contents_status')->get()->getResult();
        return $result;
    }

    //UNTUK GET PERIODE BERDASARKAN DATA YANG ADA DI CONTENTS
    public function getPeriode()
    {
        $detail = $this->db->table('contents')->select("date_format(tanggal,'%M') as bulan")
        ->groupBy("date_format(tanggal,'%M')")->get()->getResult();

        return $detail;
    }

    public function getTotalStatusContent($bulan, $status)
    {
        $detail = $this->db->table('contents')
        ->select("count(contents.id) as total")
        ->where("date_format(tanggal,'%M')", $bulan)->where('draft',$status)
        ->groupBy("draft, date_format(tanggal,'%M')")
        ->get()->getFirstRow();

        return $detail;
    }

    //QUERY UNTUK DASHBOARD REPORT
    public function getTotalKategoriBasedPeriode()
    {
        $bulan = date('m');
        $detail = $this->db->table('contents')
        ->select('nama_kategori, count(contents.id) as total')
        ->join('mastercategories','mastercategories.id = contents.kategori')
        ->where("date_format(tanggal,'%m')",$bulan)->groupBy('nama_kategori')
        ->having('count(contents.id) >= 2')
        ->get()->getResult();

        return $detail;
    }

    public function getKategoriBasedPeriode()
    {
        $bulan = date('m');
        $detail = $this->db->table('contents')
        ->select('nama_kategori, count(contents.id) as total')
        ->join('mastercategories','mastercategories.id = contents.kategori')
        ->where("date_format(tanggal,'%m')",$bulan)->where('draft', 1)
        ->groupBy('nama_kategori')->orderBy('count(contents.id)','desc')
        ->get()->getFirstRow();

        return $detail;
    }
}
