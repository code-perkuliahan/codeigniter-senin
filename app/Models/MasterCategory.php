<?php

namespace App\Models;

use CodeIgniter\Model;

class MasterCategory extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'mastercategories';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $allowedFields    = ['nama_kategori','created_at','created_by'];

    public function get_category()
    {
        $result = [];
        $query = $this->db->table('mastercategories')->get();
        foreach($query->getResult() as $key)
        {
            $result[$key->id] = $key->nama_kategori;
        }
        return $result;
    }
}
