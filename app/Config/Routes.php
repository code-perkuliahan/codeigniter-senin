<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'ReportController::index');
$routes->match(['get', 'post'],'about', 'Home::detail');
$routes->get('profile', 'Home::profile');
$routes->match(['get','post'],'profile', 'Home::profile');
$routes->get('about/(:segment)', 'Home::detailContent/$1');
$routes->get('about/download/(:segment)', 'Home::downloadFile/$1');
$routes->get('about/delete/(:segment)', 'Home::deleteBerita/$1');

$routes->match(['get','post'],'about/edit/(:segment)', 'Home::updateBerita/$1');
$routes->post('content/add', 'Home::tambahBerita');
$routes->get('export_pdf/(:segment)', 'PdfController::index/$1');
$routes->get('export_excel', 'ExportController::index');
$routes->get('dashboard_report', 'Home::index');

$routes->group('admin', function($routes){
    $routes->get('/','AuthController::index',['filter' => 'auth']);
    $routes->get('logout','AuthController::logout',['filter' => 'auth']);

    $routes->get('register', 'AuthController::register');
    $routes->get('login','AuthController::login');

    $routes->post('register', 'AuthController::register_auth');
    $routes->post('login','AuthController::auth');

    $routes->group('category', ['filter' => 'auth'], function($routes){
        $routes->get('/', 'MasterCategory::index');
        $routes->add('new','MasterCategory::create');
        $routes->add('(:segment)/edit','MasterCategory::update/$1');
        $routes->get('(:segment)/delete','MasterCategory::delete/$1');
    });
});