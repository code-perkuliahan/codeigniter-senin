-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 17, 2023 at 08:37 AM
-- Server version: 8.0.31
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `berita`
--

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text,
  `tanggal` datetime DEFAULT NULL,
  `draft` tinyint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `content`, `tanggal`, `draft`) VALUES
(1, 'judul data', 'isi data', '2023-09-25 00:00:00', 0),
(2, 'Introduction', 'Get started with Bootstrap, the world’s most popular framework for building responsive, mobile-first sites, with jsDeliver and a template starter page.', '2023-10-02 00:00:00', 0),
(3, 'Build fast, responsive sites with Bootstrap', ' Powerful, extensible, and feature-packed frontend toolkit. Build and customize with Sass, utilize prebuilt grid system and components, and bring projects to life with powerful JavaScript.', '2023-10-02 13:11:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mastercategories`
--

DROP TABLE IF EXISTS `mastercategories`;
CREATE TABLE `mastercategories` (
  `id` int NOT NULL,
  `nama_kategori` varchar(200) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `mastercategories`
--

INSERT INTO `mastercategories` (`id`, `nama_kategori`, `created_at`, `created_by`) VALUES
(2, 'Budaya Bangsa', NULL, NULL),
(3, 'Olahraga', NULL, NULL),
(4, 'Hiburan', NULL, NULL),
(5, 'Politik Internasional', NULL, NULL),
(6, 'Politik Sosial', '2023-10-16 13:53:18', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(60) NOT NULL,
  `active` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `active`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$owu8kJQbjTZNs2BJd0XBBegElylNDh2amV7EEX8rFxMpUy3skcEKC', 0),
(2, 'Super Admin', 'super admin', 'superadmin@gmail.com', '$2y$10$vh7V4ccuH9FR48joaDfDUO86qjIrWWo43fw9WaIOHmEcYM/.EyMbO', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mastercategories`
--
ALTER TABLE `mastercategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mastercategories`
--
ALTER TABLE `mastercategories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
